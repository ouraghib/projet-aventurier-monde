# Projet - _Aventuriers du Rail Autour du Monde_

## 1. Lancez la classe GameServer (src/main/java/fr/umontpellier/iut/gui/GameServer.java) afin de lancer le serveur du jeu

## 2. Cliquez sur le fichier index.html (web/index.html) et lancez le sur votre navigateur web

## 3. Vous pouvez maintenant jouer au jeu